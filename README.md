#angular-ui

Sample project for Spring Data, Spring Data Rest, AngularJs and Twitter Bootstrap

#Run app

`mvn jetty:run`

[Go to app](http://localhost:8080)

#View/Modify database content

[Go to database manager](http://localhost:8082)

Use following jdbc url `jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1`


