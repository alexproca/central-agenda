describe('EventApp controllers', function () {

    beforeEach(module('eventsApp'));

    describe('EventListController', function () {

        it('should create "events" model with 2 events', inject(function ($controller) {
            var scope = {},
                ctrl = $controller('EventListController', { $scope: scope });

            expect(scope.events.length).toBe(3);
        }));
    });
});