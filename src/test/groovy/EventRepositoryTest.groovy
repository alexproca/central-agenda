import groovy.transform.CompileStatic
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import papadie.config.AppConfig
import papadie.model.Event
import papadie.repository.EventRepository

import javax.persistence.EntityManager

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class,
        loader = AnnotationConfigContextLoader.class)
@CompileStatic
@Ignore
class EventRepositoryTest {

    @Autowired
    private AbstractEntityManagerFactoryBean emfBean;

    @Autowired
    private EventRepository eventRepository;

    @Before
    public void setUp() {
        EntityManager entityManager = emfBean.getObject().createEntityManager()

        Event e1 = new Event(name: "e1")
        Event e2 = new Event(name: "e2")
        Event e3 = new Event(name: "e3")

        entityManager.getTransaction().begin()
        entityManager.persist(e1)
        entityManager.persist(e2)
        entityManager.persist(e3)
        entityManager.getTransaction().commit()
    }

    @After
    public void tearDown() throws Exception {
        eventRepository.deleteAll();
    }

    @Test
    public void testShouldReturnEventByName() throws Exception {
        Event e = eventRepository.findByName("e2")

        assert e != null
        assert e.name == "e2"
    }

    @Test
    public void testPagingRequestReturnsExpectedNumberOfPages() throws Exception {
        PageRequest pageRequest = new PageRequest(1, 2)
        Page<Event> page = eventRepository.findAll(pageRequest)

        assert page != null
        assert page.getNumber() == 1
        assert page.size == 2
        assert page.getNumberOfElements() == 1
        assert page.getTotalElements() == 3

        println(page);
    }

    @Test
    @Ignore
    public void testCountIs3() throws Exception {
        assert eventRepository.count() == 3

        Event eventToDelete = eventRepository.findByName("e1")
        eventRepository.delete(eventToDelete)

        assert eventRepository.count() == 2
    }
}
