var eventsApp = angular.module('eventsApp', []);

eventsApp.controller('EventListController', function ($scope, $http) {

    $scope.queryEvents = function () {
        $http.get('api/v1/event').success(function (data) {
            $scope.events = data._embedded.events;
        })
    }

    $scope.deleteEvent = function (event) {
        var url = event.links[0].href;

        $http.delete(url).success(function () {
            $scope.queryEvents()
            alert("Successfully deleted " + url)
        })
    }

    $scope.editEvent = function (event) {
            $scope.currentEvent = event
        }

    $scope.queryEvents()

});