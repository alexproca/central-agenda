package papadie.utils

import com.google.common.collect.Sets
import groovy.transform.CompileStatic
import papadie.model.Event
import papadie.model.Subscriber

@CompileStatic
class DbInitializer {

    static Set<Event> someEvents() {

        Set<Subscriber> s1 = Sets.newHashSet(
                new Subscriber(name: "Eugen"),
                new Subscriber(name: "Alex")
        );

        Set<Subscriber> s2 = Sets.newHashSet(
                new Subscriber(name: "Andra"),
                new Subscriber(name: "Ciuca"),
                new Subscriber(name: "Alex 2")
        )


        Sets.newHashSet(
                new Event(name: "unu", description: "Primul Eveniment", subscribers: s1),
                new Event(name: "doi", description: "Java2Days"),
                new Event(name: "trei", description: "BJUG #18"),
                new Event(name: "patru", description: "BJUG #22", subscribers: s2),
                new Event(name: "cinci", description: "BJUG #24")
        );
    }

}
