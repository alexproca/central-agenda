package papadie.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

@Configuration
public class RestExporterWebConfiguration extends RepositoryRestMvcConfiguration {
}