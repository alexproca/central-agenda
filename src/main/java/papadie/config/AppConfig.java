package papadie.config;

import org.h2.tools.Server;
import org.hibernate.ejb.HibernatePersistence;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseFactoryBean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import papadie.model.Event;
import papadie.utils.DbInitializer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@EnableJpaRepositories(basePackages = "papadie.repository")
@EnableTransactionManagement
public class AppConfig {

    @DependsOn("h2WebServer")
    @Bean(name = "h2Server", initMethod = "start", destroyMethod = "stop")
    public Server tcpDBServer() throws SQLException {
        return Server.createTcpServer(new String[]{"-tcp", "-tcpAllowOthers", "-tcpPort", "9092"});
    }

    @Bean(name = "h2WebServer", initMethod = "start", destroyMethod = "stop")
    public Server webDBServer() throws SQLException {
        return Server.createWebServer(new String[]{"-web", "-webAllowOthers", "-webPort", "8082"});
    }

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseFactoryBean databaseFactoryBean = new EmbeddedDatabaseFactoryBean();
        databaseFactoryBean.setDatabaseType(EmbeddedDatabaseType.H2);
        databaseFactoryBean.afterPropertiesSet();
        return databaseFactoryBean.getObject();
    }

    @Bean
    public AbstractEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPackagesToScan("papadie.model");
        emf.setPersistenceProvider(new HibernatePersistence());
        emf.getJpaPropertyMap().put("hibernate.hbm2ddl.auto", "create-drop");
        emf.getJpaPropertyMap().put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        emf.getJpaPropertyMap().put("hibernate.show_sql", "true");
        emf.afterPropertiesSet();

        return emf;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactory().getObject());

        EntityManagerFactory emf = jpaTransactionManager.getEntityManagerFactory();

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        for (Event event : DbInitializer.someEvents()) {
            em.persist(event);
        }
        em.getTransaction().commit();

        return jpaTransactionManager;
    }

}
