package papadie.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import papadie.model.Subscriber;

@RepositoryRestResource
public interface SubscriberRepository extends PagingAndSortingRepository<Subscriber, Long> {
}
