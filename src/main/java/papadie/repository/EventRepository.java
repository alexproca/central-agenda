package papadie.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import papadie.model.Event;

@RepositoryRestResource(path = "event")
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {

    public Event findByName(@Param("name") String name);

}
